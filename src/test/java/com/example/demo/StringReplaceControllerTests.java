package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.StandardCharsets;


@WebMvcTest(StringReplaceController.class)
public class StringReplaceControllerTests {
    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(
            MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            StandardCharsets.UTF_8
    );

    @Autowired
    MockMvc mockMvc;

    @Test
    void strReplace_noArgs_returnsEmptyJSONTest() throws Exception {

        String testJSON = "{}";
        mockMvc.perform(MockMvcRequestBuilders
                .post("/replace")
                .param("word1", "")
                .param("word2", "")
                .content(testJSON)
                .contentType(APPLICATION_JSON_UTF8)
        )
//                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("{}"));
    }

    @Test
    void strReplace_strBodyAndParams_returnsJSONTest() throws Exception {
        String testJSON = "{\"string\" : \"Hello World\"}";
        mockMvc.perform(MockMvcRequestBuilders
                .post("/replace")
                .param("word1", "Hello")
                .param("word2", "World")
                .content(testJSON)
                .contentType(APPLICATION_JSON_UTF8)
        )
//                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.string").value(
                        "World World"))
                .andExpect(MockMvcResultMatchers.content().string("{\"string\" : \"World World\"}"));
    }
}
