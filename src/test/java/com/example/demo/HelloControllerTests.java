package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HelloController.class)
public class HelloControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void sayHello_noArgs_returnsHelloWorldTest() throws Exception {
        mockMvc.perform(get("/hello"))
//                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("Hello World"));


    }

    @Test
    void sayHello_nameArg_returnsHelloNameTest() throws Exception {
        mockMvc.perform(get("/hello?name=Andy"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("Hello Andy"));
    }
}
