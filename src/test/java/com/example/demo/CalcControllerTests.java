package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CalcController.class)
public class CalcControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void calc_noArgs_returnsANumTest() throws Exception {
        mockMvc.perform(get("/calculate"))
                .andExpect(status().isOk())
                .andExpect(content().string("0.0"));
    }

    @Test
    void calc_twoNumsAndOpArgs_returnsANumTest() throws Exception {
        mockMvc.perform(get("/calculate").param("num1", "2").param("num2",
                "2").param("operator", "+"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("4.0"));
    }

    @Test
    void calc_arrayNumsAndOpArgs_returnsANumTest() throws Exception {
        mockMvc.perform(get("/calculateArrays").param("nums", "1, 2, 3").param(
                "operator", "+"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string("6.0"));
    }
}
