package com.example.demo;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StringReplaceController {

    @PostMapping("/replace")
    public String replace(@RequestBody String string, String word1,
                          String word2) {
        var result = string.replace(word1, word2);
        return string.length() == 0 ? "{}" : result;
    }
}
