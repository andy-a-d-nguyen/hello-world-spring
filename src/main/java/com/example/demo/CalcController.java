package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalcController {

    @GetMapping("/calculate")
    public double calculate(@RequestParam(defaultValue = "0") double num1,
                            @RequestParam(defaultValue = "0") double num2,
                            @RequestParam(defaultValue = "+") Character operator) {
        switch (operator) {
            case '+':
                return num1 + num2;
            case '-':
                return num1 - num2;
            case '*':
                return num1 * num2;
            default:
                return num1 / num2;
        }
    }

    @GetMapping("/calculateArrays")
    public double calculate(@RequestParam(defaultValue = "nums") double[] nums,
                            @RequestParam(defaultValue = "+") Character operator) {
        double result = 0;
        switch (operator) {
            case '+':
                for (double num : nums) {
                    result += num;
                }
                break;
            case '-':
                for (double num : nums) {
                    result -= num;
                }
                break;
            case '*':
                for (double num : nums) {
                    result *= num;
                }
                break;
            default:
                for (double num : nums) {
                    result /= num;
                }
        }
        return result;
    }
}
