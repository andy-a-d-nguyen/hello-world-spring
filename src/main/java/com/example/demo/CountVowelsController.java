package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CountVowelsController {

    @GetMapping("/count")
    public int countVowels(@RequestParam(defaultValue = "") String string) {
        int count = 0;
        for (int i = 0; i < string.length(); i++) {
            if ("aeiouAEIOU".indexOf(string.charAt(i)) != -1) {
                count++;
            }
        }
        return count;
    }
}
